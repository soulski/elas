#!/usr/bin/env node

const program = require('commander'),
      fs = require('fs'),
      yaml = require('js-yaml'),
      elas = require('./index.js');

program
    .version('0.1.0')
    .command('build [config file]')
    .alias('b')
    .option('-c --config <path>', 'Config file path')
    .description('build database from scratch from config file')
    .action((actionPath, options) => { 
        const actionStr = fs.readFileSync(actionPath, 'utf8'),
              { actions } = yaml.safeLoad(actionStr);


        const configPath = options.config;
        let config = {
            client: {
                host: ['127.0.0.1'],
                port: 9200
            }
        };

        if (configPath) {
            config  = yaml.safeLoad(fs.readFileSync(configPath, 'utf8'));
        }

        elas.build(config, actions);
    });

program.parse(process.argv);
