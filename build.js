const _ = require('lodash'),
      es = require('elasticsearch');

const Promise = require('bluebird');

const deleteIndice = (cli, command) => {
    const names = command.filters.map(filter => filter.value);

    const pullExistsName = (names, exists) => {
        exists = _.isArray(exists) ? exists : [exists];

        const existsNames = []; 
        for (let index = 0; index < exists.length; index++) {
            if (exists[index]) {
                existsNames.push(names[index]);
            }
        }

        return existsNames;
    }

    return cli.indices.exists({
        index: names    
    }).then(exists => {
        existsNames = pullExistsName(names, exists)

        if (existsNames.length <= 0) {
            return Promise.resolve({});
        }

        return cli.indices.delete({
            index: existsNames    
        })
        .then(() => {
            existsNames.forEach(name => {
                console.log(`${name} indice has been deleted`);  
            });

            return Promise.resolve(existsNames);
        });   
    })
}

const createIndice = (cli, command) => {
    const { options } = command;

    return cli.indices.create({
        index: options.name,
        body: options.extra_settings
    })
    .then(() => {
        console.log(`${options.name} indice has been created`);
        return Promise.resolve(options.name);
    });
}

const Actions = {
    'delete_indices': deleteIndice,
    'create_index': createIndice
}

const getParam = (config, param) => {
    const value = config[param];
    const found = value.match(/^\$\{(.*)\}$/);

    if (!found || found.length === 0) {
        return value;
    }

    return process.env[found[1]];

}

const buildCli = (config) => {
    const { client } = config;
    let hosts;
    
    if (client.port) {
        hosts = client.hosts.map( host => `${host}:${client.port}` );
    }
    else {
        hosts = client.hosts.map( host => `${host}` );
    }

    let options = {
        host: hosts     
    };

    if (client.aws_key && client.aws_secret_key) {
        options.connectionClass = require('http-aws-es');
        options.amazonES = {
            region: getParam(client, 'aws_region'),
            accessKey: getParam(client, 'aws_key'),
            secretKey: getParam(client, 'aws_secret_key')
        };
    }

    return new es.Client(options);
}

const build = (config, actions) => {
    const keys = _.keys(actions);
    const cli = buildCli(config);

    const tasks = keys.map(key => {
        const action = actions[key];

        return () => {
            return Actions[action.action](cli, action);
        };
    });

    Promise.each(tasks, (task) => {
        return task(); 
    })
    .then((data) => {
        console.log('Build finished.'); 
    })
    .catch(e => {
        console.log(e);    
    });
}

module.exports = build;
